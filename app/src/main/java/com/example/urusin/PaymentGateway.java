package com.example.urusin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PaymentGateway extends AppCompatActivity {

    TopUp topUp = new TopUp();

    AcceptProject acceptProject = new AcceptProject();
    DetailProduct detailProduct = new DetailProduct();
    MyTransaksiUnpaid myTransaksiUnpaid = new MyTransaksiUnpaid();
    Button button_back_to_home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_gateway);

        if (topUp.urlredirect != "1"){


            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(topUp.urlredirect));
            topUp.urlredirect = "1";
            startActivity(intent);
        }


        if (acceptProject.urlredirects != "1"){
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(acceptProject.urlredirects));
            acceptProject.urlredirects = "1";
            startActivity(intent);
        }

        if (detailProduct.urlredirectss != "1"){

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(detailProduct.urlredirectss));
            detailProduct.urlredirectss = "1";

            startActivity(intent);
        }


        if (myTransaksiUnpaid.url_ != "1"){

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(myTransaksiUnpaid.url_));
            myTransaksiUnpaid.url_ = "1";

            startActivity(intent);
        }

        button_back_to_home = findViewById(R.id.button_back_to_home);
        button_back_to_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intents = new Intent(PaymentGateway.this, MainActivity.class);
                startActivity(intents);
                finish();
            }
        });

    }
}
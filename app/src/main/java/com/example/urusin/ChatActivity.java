package com.example.urusin;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class ChatActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private ChatAdapter adapter;
    private List<Message> messages;

    MyBidTab myBidTab = new MyBidTab();
    MyBidOther myBidOther = new MyBidOther();
    int count = 0;

    boolean finishfab = false; // Ganti dengan kondisi Anda
    boolean accfab = false; // Ganti dengan kondisi Anda
    public static String id_for_bid = "";

    EditText edit_text_message;
    String send_str;
    Button btn_send;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        messages = new ArrayList<>();
        messages = new ArrayList<>();

        // Memanggil metode untuk mengambil data pesan
        ListData();
        edit_text_message = findViewById(R.id.edit_text_message);
        btn_send = findViewById(R.id.button_send);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInputan();
            }
        });

        checkButton();

    }
    private void checkButton() {
        // Kondisi untuk menampilkan atau menyembunyikan FloatingActionButton
        FloatingActionButton fab = findViewById(R.id.accept);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                id_for_bid = myBidTab.transaksi_id;
                Intent intents = new Intent(ChatActivity.this, AcceptProject.class);
                startActivity(intents);
                finish();
            }
        });

        FloatingActionButton fabfinish = findViewById(R.id.finish);

        fabfinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                id_for_bid = myBidTab.transaksi_id;

                Intent intents = new Intent(ChatActivity.this, FinishProject.class);
                startActivity(intents);
            }
        });
        if (accfab) {
            fab.setVisibility(View.VISIBLE); // Tampilkan FloatingActionButton
        } else {
            fab.setVisibility(View.GONE); // Sembunyikan FloatingActionButton
        }


        // Kondisi untuk menampilkan atau menyembunyikan FloatingActionButton

        if (finishfab) {
            fabfinish.setVisibility(View.VISIBLE); // Tampilkan FloatingActionButton
        } else {
            fabfinish.setVisibility(View.GONE); // Sembunyikan FloatingActionButton
        }
    }
    private void checkInputan() {
        send_str = edit_text_message.getText().toString();
        if (send_str.isEmpty()){
            alertFail("All Input is Required");

        }else{
            SimpanData();

        }
    }
    private void SimpanData() {

        JSONObject params = new JSONObject();
        try {
            params.put("description", send_str);

        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        Log.d("logadi", params.toString());

        String data = params.toString();
        String url = getString(R.string.api_server)+"discuss/" + myBidTab.transaksi_id;
        Log.d("logadi", url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("logadi", "cekmasuk");

                showProgressDialog();
                Http http = new Http(ChatActivity.this, url);
                http.setMethod("post");
                http.setToken(true);
                http.setData(data);
                http.send();
                Log.d("logadi", http.getResponse());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        Integer code = http.getStatuscode();

                        Log.d("logadi", code.toString());

                        if (code == 200){
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                JSONObject data = response.getJSONObject("data");

                                ListData();
                                edit_text_message.setText("");

                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                String msg = response.getString("message");
                                alertFail(msg);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        }).start();

    }

    private String currentUserId = "";
    private String bideruser = "0";
    private void ListData() {

        String url = getString(R.string.api_server)+"discuss/"+ myBidTab.transaksi_id;
        Log.d("logadi", url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("logadi", "cekmasukdialog");
                showProgressDialog();
                Http http = new Http(ChatActivity.this, url);
                http.setToken(true);
                http.send();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        Integer code = http.getStatuscode();
                        Log.d("logadi", code.toString());

                        Log.d("logadi", "cek_responses:");
                        Log.d("logadi", http.getResponse().toString());

                        if (code == 200){
                            NumberFormat rupiah = NumberFormat.getInstance(Locale.GERMANY);
                            try {
                                Log.d("logadi","data-data ");
                                messages.clear();
                                JSONObject response = new JSONObject(http.getResponse());
                                JSONObject data = response.getJSONObject("data");
                                JSONArray dataArray = data.getJSONArray("discuss");
                                JSONObject user = data.getJSONObject("project");
                                currentUserId = user.getString("created_by");
                                bideruser = user.getString("chosen_id");

                                Log.d("logadi",bideruser);
                                Log.d("logadi",user.getString("status"));

                                if(myBidOther.owner.toString() == "1"){
                                    if (bideruser.equals("null")) {
                                        accfab = true;
                                    }
                                }else{
                                    if (bideruser.equals(MyBidTab.user_id)) {
                                        if (user.getString("status").equals("FINISH")){
                                            finishfab = false;
                                        }else if (user.getString("status").equals("RUNNING")){
                                            finishfab = true;
                                        }

                                    } else {
                                        finishfab = false;
                                    }
                                }




                                checkButton();

                                count = dataArray.length();
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject discussObject = dataArray.getJSONObject(i);
                                    String description = discussObject.getString("description");
                                    String userId = discussObject.getString("user_id");

                                    // Buat objek Message dengan data yang diperoleh
                                    Message message = new Message(description, userId);
                                    messages.add(message);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            adapter = new ChatAdapter(messages, currentUserId);
                            recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                            if(count > 10){
                                recyclerView.smoothScrollToPosition(adapter.getItemCount() - 1); // Scroll ke posisi terakhir

                            }

                        }else if (code == 401){
                            Intent intent = new Intent(ChatActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }else{
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                String msg = response.getString("message");
                                alertFail(msg);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });


            }
        }).start();


    }





    private void alertFail(String s) {
        new AlertDialog.Builder(this)
                .setTitle("Failed")
                .setIcon(R.drawable.warning)
                .setMessage(s)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }

    private AlertDialog progressDialog;
    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog = new AlertDialog.Builder(ChatActivity.this)
                        .setTitle("Loading")
                        .setView(R.layout.loading)
                        .setCancelable(false)
                        .show();
            }
        });
    }

    private void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }
}
package com.example.urusin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class DataProject extends AppCompatActivity {
    SwipeRefreshLayout swipeRefreshLayout;

    // variable untuk list data
    ListView list_project;

    public static String project_id;

    TabProject tabProject = new TabProject();
    ArrayList<HashMap<String, String>> datas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_project);

        androidx.appcompat.widget.Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("List Project");

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ListDataProject();
            }
        });

        list_project = (ListView) findViewById(R.id.list_project);
        project_id = "";

        list_project.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                project_id = ((TextView) view.findViewById(R.id.text_transaksi_id)).getText().toString();
                Intent intent = new Intent(DataProject.this, DetailProject.class);
                startActivity(intent);

            }
        });

        ListDataProject();
    }



    private void ListDataProject() {

        swipeRefreshLayout.setRefreshing(false);
        datas.clear(); list_project.setAdapter(null);

        String url = getString(R.string.api_server)+"list_project/" + tabProject.transaksi_id;
        Log.d("logadi", url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("logadi", "cekmasukdialog");
                showProgressDialog();
                Http http = new Http(DataProject.this, url);
                http.setToken(true);
                http.send();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        Integer code = http.getStatuscode();
                        Log.d("logadi", code.toString());

                        Log.d("logadi", "cek_responses:");
                        Log.d("logadi", http.getResponse().toString());

                        if (code == 200){

                            NumberFormat rupiah = NumberFormat.getInstance(Locale.GERMANY);
                            try {
                                Log.d("logadi","data-data ");

                                JSONObject response = new JSONObject(http.getResponse());
//                                JSONObject data = response.getJSONObject("data");
                                JSONArray dataArray = response.getJSONArray("data");

                                if (dataArray.length() > 0) {
                                    for (int i = 0; i < dataArray.length(); i++) {
                                        // Mendapatkan objek pertama dari array data
                                        JSONObject dataObject = dataArray.getJSONObject(i);

                                        // Mengakses nilai-nilai di dalam objek data
                                        String id = dataObject.getString("id");
                                        String name = dataObject.getString("name");
                                        String description = dataObject.getString("description");
                                        String city = dataObject.getString("city");
                                        String price_form = rupiah.format(dataObject.getInt("price_form"));
                                        // ...
                                        String details = "KLIK DETAIL";

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("id", id);
                                        map.put("name", name);
                                        map.put("description",description);
                                        map.put("city",city);
                                        map.put("details",details);
                                        map.put("price_form","Rp. "+price_form);

                                        datas.add(map);
                                    }
                                }

                                SimpleAdapter simpleAdapter = new SimpleAdapter(DataProject.this, datas, R.layout.list_project ,
                                        new String[]{ "id", "name", "description", "price_form","city", "details" },
                                        new int[]{R.id.text_transaksi_id, R.id.name_project, R.id.deskripsi_project, R.id.harga_project, R.id.kota, R.id.klik_detail}) {

                                };

                                list_project.setAdapter(simpleAdapter);

                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }else if (code == 401){
                            Intent intent = new Intent(DataProject.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }else{
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                String msg = response.getString("message");
                                alertFail(msg);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });


            }
        }).start();

    }

    private AlertDialog progressDialog;
    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog = new AlertDialog.Builder(DataProject.this)
                        .setTitle("Loading")
                        .setView(R.layout.loading)
                        .setCancelable(false)
                        .show();
            }
        });
    }

    private void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }
    private void alertFail(String s) {
        new AlertDialog.Builder(DataProject.this)
                .setTitle("Failed")
                .setIcon(R.drawable.warning)
                .setMessage(s)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }
}
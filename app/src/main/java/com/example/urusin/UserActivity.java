package com.example.urusin;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.nareshchocha.filepickerlibrary.models.DocumentFilePickerConfig;
import com.nareshchocha.filepickerlibrary.ui.FilePicker;
import com.nareshchocha.filepickerlibrary.utilities.appConst.Const;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UserActivity extends AppCompatActivity {

    LocalStorage localStorage;
    EditText name,email,alamat,phone,saldo,address, city,nama_rekening,no_rekening;
    Button button, btn_topup, btn_update,btn_withdraw;
    public static String mysaldo;
    String nama, hp,addresstxt, citytxt,nama_rekeningtxt, no_rekeningtxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        initView();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
            if(!Environment.isExternalStorageManager())
            {
                Intent permissionIntent = new Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                startActivity(permissionIntent);
            }
        }

        androidx.appcompat.widget.Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        saldo = findViewById(R.id.saldo);

        address = findViewById(R.id.address);
        city = findViewById(R.id.city);
        no_rekening = findViewById(R.id.no_rekening);

        nama_rekening = findViewById(R.id.nama_rekening);

        btn_topup = findViewById(R.id.btn_topup);
        btn_topup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(UserActivity.this, TopUp.class);
                startActivity(intent);
            }
        });

        btn_update = findViewById(R.id.btn_update);
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                checkInputan();
            }
        });


        btn_withdraw = findViewById(R.id.btn_withdraw);
        btn_withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                mysaldo = saldo.getText().toString();

                Intent intent = new Intent(UserActivity.this, Withdraw.class);
                startActivity(intent);
//                UpdateUserwith();
            }
        });

        button = findViewById(R.id.btn_logout);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });

    }

    private void checkInputan() {
        nama = name.getText().toString();
        hp = phone.getText().toString();
        citytxt = city.getText().toString();
        addresstxt = address.getText().toString();
        no_rekeningtxt = no_rekening.getText().toString();
        nama_rekeningtxt = nama_rekening.getText().toString();
        if (citytxt.isEmpty() ||addresstxt.isEmpty() || nama.isEmpty()|| no_rekeningtxt.isEmpty()|| nama_rekeningtxt.isEmpty()){
            alertFail("All Input is Required");

        }else{
            UpdateUser();

        }
    }

    private void UpdateUser() {

        JSONObject params = new JSONObject();
        try {
            params.put("name", nama);
            params.put("phone_no", hp);

            params.put("address", addresstxt);
            params.put("city", citytxt);
            params.put("no_rekening", no_rekeningtxt);
            params.put("nama_rekening", nama_rekeningtxt);
            JSONArray jsonArray = new JSONArray();

            for (MediaFile mediaFile : medias) {
                String encodeFile = "";
                Uri urimedia = mediaFile.getUri();
                File file = new File(urimedia.getPath().trim());
                if(file.exists()){
                    Log.e("TAG","file found ");
                    int size = (int) file.length();
                    byte[] bytes = new byte[size];
                    try {
                        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                        buf.read(bytes, 0, bytes.length);
                        buf.close();
                        encodeFile = Base64.encodeToString(bytes, Base64.DEFAULT);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                String fileName = file.getName();
                JSONObject objectFile = new JSONObject();
                objectFile.put("filename", fileName);
                objectFile.put("data",encodeFile);
                jsonArray.put(objectFile);

            }
            params.put("files", jsonArray);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        Log.d("logadi", params.toString());

        String data = params.toString();
        String url = getString(R.string.api_server)+"profil";
        Log.d("logadi", url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("logadi", "cekmasuk");

                showProgressDialog();
                Http http = new Http(UserActivity.this, url);
                http.setMethod("put");
                http.setToken(true);
                http.setData(data);
                http.send();
                Log.d("logadi", http.getResponse());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        Integer code = http.getStatuscode();

                        Log.d("logadi", code.toString());

                        if (code == 200){
                            getUser();

                            String msg = "Success Update Data";
                            successAlert(msg);
                        }else{
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                String msg = response.getString("message");
                                alertFail(msg);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        }).start();

    }

    private void UpdateUserwith() {

        JSONObject params = new JSONObject();
        try {
            params.put("withdraw", "yes");
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        Log.d("logadi", params.toString());

        String data = params.toString();
        String url = getString(R.string.api_server)+"profil";
        Log.d("logadi", url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("logadi", "cekmasuk");

                showProgressDialog();
                Http http = new Http(UserActivity.this, url);
                http.setMethod("put");
                http.setToken(true);
                http.setData(data);
                http.send();
                Log.d("logadi", http.getResponse());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        Integer code = http.getStatuscode();

                        Log.d("logadi", code.toString());

                        if (code == 200){
                            getUser();

                            String msg = "Success Withdraw";
                            successAlert(msg);
                        }else{
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                String msg = response.getString("message");
                                alertFail(msg);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        }).start();

    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
    @Override
    public void onResume() {
        super.onResume();
        getUser();


    }
    private MediaAdapter mediaAdapter;
    private ArrayList<MediaFile> medias = new ArrayList<>();
    private Intent filePicker;
    private static String TAG = FinishProject.class.getSimpleName();


    private void initView() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        mediaAdapter = new MediaAdapter(this, medias);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mediaAdapter);
        List<String> mimes = new ArrayList<>();
        mimes.add("*/*");
        filePicker = new FilePicker.Builder(UserActivity.this)
                .addPickDocumentFile(new DocumentFilePickerConfig(
                        R.drawable.ic_launcher_background,// DrawableRes Id
                        "File Media",
                        true,// set Multiple pick file
                        5, // set maximum files
                        mimes,// added Multiple MimeTypes
                        null, // set Permission ask Title
                        null,// set Permission ask Message
                        null,// set Permission setting Title
                        null// set Permission setting Messag
                )).build();

        ImageView profile_image = findViewById(R.id.profile_image);
        profile_image.setOnClickListener(v -> launcherMedia.launch(filePicker));
    }

    ActivityResultLauncher<Intent> launcherMedia = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    medias.clear();
                    Intent intent = result.getData();
                    assert intent != null;
                    try {
                        // implement for single file
                        String filePath = intent.getStringExtra(Const.BundleExtras.FILE_PATH); // for single
                        medias.add(new MediaFile(Uri.parse(filePath)));
                    } catch (Exception e) {
                        ArrayList<String> listPatch = intent.getStringArrayListExtra(Const.BundleExtras.FILE_PATH_LIST);
                        for (String filePath : listPatch) {
                            medias.add(new MediaFile(Uri.parse(filePath)));
                        }
                    }
                    mediaAdapter.notifyDataSetChanged();
                }
            });


    private void getUser() {
        String url = getString(R.string.api_server)+"profil";
        Log.d("logadi", url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                 Log.d("logadi", "cekmasuk");
                showProgressDialog();
                Http http = new Http(UserActivity.this, url);
                http.setToken(true);
                http.send();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        Integer code = http.getStatuscode();
                        Log.d("logadi", code.toString());

                        Log.d("logadi", "cek_response:");
                        Log.d("logadi", http.getResponse().toString());
                        if (code == 200){
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                JSONObject data = response.getJSONObject("data");

                                String names = data.getString("name");
                                String emails = data.getString("email");
                                String phone_nos = data.getString("phone_no");
                                String addresss = data.getString("address");
                                String citys = data.getString("city");
                                String no_rekenings = data.getString("no_rekening");
                                String nama_rekenings = data.getString("nama_rekening");

                                name.setText(names);
                                email.setText(emails);
                                phone.setText(phone_nos);
                                address.setText(addresss);
                                city.setText(citys);
                                no_rekening.setText(no_rekenings);
                                nama_rekening.setText(nama_rekenings);
                                String pictures = data.getString("picture");


                                ImageView imageCategory = findViewById(R.id.profile_image);
                                String imageUrl = pictures;
                                Log.d("logadi", pictures);

                                Picasso.get()
                                        .load(imageUrl)
                                        .placeholder(R.mipmap.ic_launcher) // gambar placeholder
                                        .error(R.mipmap.ic_launcher) // gambar yang ditampilkan jika terjadi kesalahan
                                        .into(imageCategory);

                                JSONObject saldos = data.getJSONObject("saldo");
                                String saldoss = saldos.getString("saldo");
                                saldo.setText(saldoss);


                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }else if (code == 401){
                            Intent intent = new Intent(UserActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }else{
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                String msg = response.getString("message");
                                alertFail(msg);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });


            }
        }).start();


    }


    private void logout() {
        String url = getString(R.string.api_server)+"logout";
        Log.d("logadi", url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("logadi", "cekmasuk");
                showProgressDialog();
                Http http = new Http(UserActivity.this, url);
                http.setToken(true);
                http.setMethod("post");
                http.send();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        Integer code = http.getStatuscode();
                        Log.d("logadi", code.toString());

                        Log.d("logadi", "cek_response:");
                        Log.d("logadi", http.getResponse().toString());
                        if (code == 200){

                            Intent intent = new Intent(UserActivity.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK); // Tambahkan flag ini
                             startActivity(intent);
                            finish();

                        }else{
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                String msg = response.getString("message");
                                alertFail(msg);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });


            }
        }).start();


    }

    private AlertDialog progressDialog;
    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog = new AlertDialog.Builder(UserActivity.this)
                        .setTitle("Loading")
                        .setView(R.layout.loading)
                        .setCancelable(false)
                        .show();
            }
        });
    }

    private void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }
    private void alertFail(String s) {
        new AlertDialog.Builder(this)
                .setTitle("Failed")
                .setIcon(R.drawable.warning)
                .setMessage(s)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }

    private void successAlert(String s) {
        new AlertDialog.Builder(this)
                .setTitle("Success")
                .setIcon(R.drawable.checklis)
                .setMessage(s)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }
}
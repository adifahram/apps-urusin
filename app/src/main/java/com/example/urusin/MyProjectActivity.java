package com.example.urusin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;

public class MyProjectActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager2 viewPager2;
    ViewPagerAdapterMyProject viewPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_project);


        tabLayout = findViewById(R.id.tablaymyproject);
        viewPager2 = findViewById(R.id.view_pager_myproject);

        viewPagerAdapter = new ViewPagerAdapterMyProject(this);
        viewPager2.setAdapter(viewPagerAdapter);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager2.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                tabLayout.getTabAt(position).select();
            }
        });


        androidx.appcompat.widget.Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        BottomNavigationView bottomNavigationView = findViewById(R.id.menu_bawah);
        bottomNavigationView.setSelectedItemId(R.id.projects);

        bottomNavigationView.setOnItemSelectedListener(item -> {
            if (item.getItemId() == R.id.homes){
                Intent intent = new Intent(MyProjectActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }else if (item.getItemId() == R.id.profiles){
                Intent intent = new Intent(MyProjectActivity.this, UserActivity.class);
                startActivity(intent);

            }else if (item.getItemId() == R.id.bids){
                Intent intent = new Intent(MyProjectActivity.this, MyBidActivity.class);
                startActivity(intent);
                finish();

            }else if (item.getItemId() == R.id.transaksis){
                Intent intent = new Intent(MyProjectActivity.this, MyTransaksiActivity.class);
                startActivity(intent);
                finish();

            }
            return true;
        });
    }
}
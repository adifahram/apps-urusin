package com.example.urusin;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class MyBidTab extends Fragment {
    SwipeRefreshLayout swipeRefreshLayout;

    // variable untuk list data
    ListView list_bid;

    public static String transaksi_id, user_id = "";

    ArrayList<HashMap<String, String>> datas = new ArrayList<>();

    public MyBidTab() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_bid_tab, container,false);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ListData();
            }
        });

        list_bid = (ListView) view.findViewById(R.id.list_bid);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ListData();
            }
        });

        list_bid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                transaksi_id = ((TextView) view.findViewById(R.id.text_transaksi_id)).getText().toString();
                user_id = ((TextView) view.findViewById(R.id.user_id)).getText().toString();
                startActivity(new Intent(getActivity(), ChatActivity.class));
            }
        });

//        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(getActivity(), MyProjectAdd.class));
//
//            }
//        });

        // Inflate the layout for this fragment
        return view;
    }


    private void ListData() {
        swipeRefreshLayout.setRefreshing(false);
        datas.clear(); list_bid.setAdapter(null);
        String url = getString(R.string.api_server)+"my_bid";
        Log.d("logadi", url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("logadi", "cekmasukdialog");
                showProgressDialog();
                Http http = new Http(getActivity(), url);
                http.setToken(true);
                http.send();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        Integer code = http.getStatuscode();
                        Log.d("logadi", code.toString());

                        Log.d("logadi", "cek_responses:");
                        Log.d("logadi", http.getResponse().toString());

                        if (code == 200){

                            NumberFormat rupiah = NumberFormat.getInstance(Locale.GERMANY);
                            try {

                                JSONObject response = new JSONObject(http.getResponse());
                                JSONArray dataArray = response.getJSONArray("data");

                                if (dataArray.length() > 0) {
                                    for (int i = 0; i < dataArray.length(); i++) {
                                        // Mendapatkan objek pertama dari array data
                                        JSONObject dataObject = dataArray.getJSONObject(i);
                                        Log.d("logadi",dataObject.toString());

                                        JSONObject userObject = dataObject.getJSONObject("project");
                                        Log.d("logadi",userObject.toString());



                                        String id = dataObject.getString("id");
                                        String description = dataObject.getString("description");
                                        String city = dataObject.getString("city");
                                        String price_form = rupiah.format(dataObject.getInt("amount_bid"));
                                        String status = dataObject.getString("status");
                                        String name = userObject.getString("name");
                                        String created_by = dataObject.getString("created_by");



                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("id", id);
                                        map.put("name", name);
                                        map.put("description",description);
                                        map.put("city",city);
                                        map.put("status",status);
                                        map.put("created_by",created_by);
                                        map.put("price_form","Rp. "+price_form);

                                        datas.add(map);
                                        Log.d("logadi",datas.toString());

                                    }
                                }

                                SimpleAdapter simpleAdapter = new SimpleAdapter(getActivity(), datas, R.layout.list_bid ,
                                        new String[]{ "created_by","id", "name", "description", "price_form","status","city" },
                                        new int[]{R.id.user_id,R.id.text_transaksi_id, R.id.name_project, R.id.deskripsi_project, R.id.harga_project, R.id.status, R.id.kota}) {

                                };

                                list_bid.setAdapter(simpleAdapter);

                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }else if (code == 401){
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        }else{
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                String msg = response.getString("message");
                                alertFail(msg);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });


            }
        }).start();


    }

    private AlertDialog progressDialog;
    private void showProgressDialog() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog = new AlertDialog.Builder(getActivity())
                        .setTitle("Loading")
                        .setView(R.layout.loading)
                        .setCancelable(false)
                        .show();
            }
        });
    }

    private void hideProgressDialog() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }
    private void alertFail(String s) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Failed")
                .setIcon(R.drawable.warning)
                .setMessage(s)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }
}
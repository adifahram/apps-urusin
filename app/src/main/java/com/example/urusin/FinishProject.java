package com.example.urusin;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nareshchocha.filepickerlibrary.models.DocumentFilePickerConfig;
import com.nareshchocha.filepickerlibrary.ui.FilePicker;
import com.nareshchocha.filepickerlibrary.utilities.appConst.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FinishProject extends AppCompatActivity {
    private MediaAdapter mediaAdapter;
    private ArrayList<MediaFile> medias = new ArrayList<>();
    private Intent filePicker;
    MyBidTab myBidTab = new MyBidTab();
    EditText keterangan;
    String keterangans;

    private static String TAG = FinishProject.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_project);

        initView();
        keterangan = findViewById(R.id.keterangan);
        // required for android 11 above
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
            if(!Environment.isExternalStorageManager())
            {
                Intent permissionIntent = new Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                startActivity(permissionIntent);
            }
        }

    }

    // initializing ui
    private void initView() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        mediaAdapter = new MediaAdapter(this, medias);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mediaAdapter);
        List<String> mimes = new ArrayList<>();
        mimes.add("*/*");
        filePicker = new FilePicker.Builder(FinishProject.this)
                .addPickDocumentFile(new DocumentFilePickerConfig(
                        R.drawable.ic_launcher_background,// DrawableRes Id
                        "File Media",
                        true,// set Multiple pick file
                        5, // set maximum files
                        mimes,// added Multiple MimeTypes
                        null, // set Permission ask Title
                        null,// set Permission ask Message
                        null,// set Permission setting Title
                        null// set Permission setting Messag
                )).build();
        Button btnUpload = findViewById(R.id.btnUpload);
        btnUpload.setOnClickListener(v -> SimpanData());

        Button btnPicker = findViewById(R.id.btnChooser);
        btnPicker.setOnClickListener(v -> launcherMedia.launch(filePicker));
    }

    private void SimpanData() {
        String deskripsi_str = keterangan.getText().toString();
        JSONObject params = new JSONObject();
        try {
            params.put("description", deskripsi_str);
            JSONArray jsonArray = new JSONArray();

            for (MediaFile mediaFile : medias) {
                String encodeFile = "";
                Uri urimedia = mediaFile.getUri();
                File file = new File(urimedia.getPath().trim());
                if(file.exists()){
                    Log.e("TAG","file found ");
                    int size = (int) file.length();
                    byte[] bytes = new byte[size];
                    try {
                        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                        buf.read(bytes, 0, bytes.length);
                        buf.close();
                        encodeFile = Base64.encodeToString(bytes, Base64.DEFAULT);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                String fileName = file.getName();
                JSONObject objectFile = new JSONObject();
                objectFile.put("filename", fileName);
                objectFile.put("data",encodeFile);
                jsonArray.put(objectFile);

            }
            params.put("files", jsonArray);

        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        String data = params.toString();

        String url = getString(R.string.api_server)+"finish/" + myBidTab.transaksi_id;
        Log.d("logadi","url: "+ url);

        Log.d("logadi","param"+params.toString());
        showProgressDialog();

        new Thread(() -> {
            Http http = new Http(FinishProject.this, url);
            http.setMethod("post");
            http.setToken(true);
            http.setData(data);
            http.uploadFile();

            runOnUiThread(() -> {
                hideProgressDialog();

                Integer code = http.getStatuscode();
                Log.d("logadi","response: "+ http.getResponse());

                if (code == 200){
                    Intent intents = new Intent(FinishProject.this, MainActivity.class);
                    startActivity(intents);
                    finish();
                }
                Toast.makeText(getApplicationContext(), code.toString(), Toast.LENGTH_SHORT).show();
            });
        }).start();
    }


    ActivityResultLauncher<Intent> launcherMedia = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    medias.clear();
                    Intent intent = result.getData();
                    assert intent != null;
                    try {
                        // implement for single file
                        String filePath = intent.getStringExtra(Const.BundleExtras.FILE_PATH); // for single
                        medias.add(new MediaFile(Uri.parse(filePath)));
                    } catch (Exception e) {
                        ArrayList<String> listPatch = intent.getStringArrayListExtra(Const.BundleExtras.FILE_PATH_LIST);
                        for (String filePath : listPatch) {
                            medias.add(new MediaFile(Uri.parse(filePath)));
                        }
                    }
                    mediaAdapter.notifyDataSetChanged();
                }
            });


    private void alertFail(String s) {
        new AlertDialog.Builder(this)
                .setTitle("Failed")
                .setIcon(R.drawable.warning)
                .setMessage(s)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }

    private AlertDialog progressDialog;
    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog = new AlertDialog.Builder(FinishProject.this)
                        .setTitle("Loading")
                        .setView(R.layout.loading)
                        .setCancelable(false)
                        .show();
            }
        });
    }

    private void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }
}
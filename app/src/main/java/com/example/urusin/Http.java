package com.example.urusin;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Http {
    private static String TAG = Http.class.getSimpleName();

    Context context;
    private String url, method="GET", data=null, response=null;
    private Integer statuscode = 0;
    private Boolean token = false;
    private LocalStorage localStorage;

    public Http(Context context, String url){
        this.context = context;
        this.url = url;
        localStorage = new LocalStorage(context);

    }

    public void setMethod(String method) {
        this.method = method.toUpperCase();
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setToken(Boolean token) {
        this.token = token;
    }

    public String getResponse() {
        return response;
    }

    public Integer getStatuscode() {
        return statuscode;
    }

    public void send(){
        try {
            Log.d("logadi", "masuk method send:");

            URL surl = new URL(url);
            HttpURLConnection connection = null;
            connection = (HttpURLConnection) surl.openConnection();
            connection.setRequestMethod(method);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("X-Requested-With", "XMLHttpRequest");

            if (token){
                connection.setRequestProperty("Authorization", "Bearer "+localStorage.getToken());
            }
            if (! method.equals("GET")){
                connection.setDoOutput(true);
            }
            Log.d("logadi", "masuk method get:");

            if(data != null){
                Log.d("logadi", "data");

                OutputStream os = connection.getOutputStream();
                os.write(data.getBytes());
                os.flush();
                os.close();
            }

            statuscode = connection.getResponseCode();
            Log.d("logadi", "conecting"+ statuscode.toString());


            InputStreamReader isr;
            if (statuscode >= 200 && statuscode <= 299){
                isr = new InputStreamReader(connection.getInputStream());

            }else {
                isr = new InputStreamReader(connection.getErrorStream());
            }

            BufferedReader br = new BufferedReader(isr);
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null){
                sb.append(line);
            }
            Log.d("logadi", "bawah");

            br.close();
            response = sb.toString();

        } catch (IOException e) {
           e.printStackTrace();
        }
    }


    /*
     * Upload files
     * */
    public void uploadFile(){
        try {
            Log.d(TAG, "uploadFile request");
            URL surl = new URL(url);
            HttpURLConnection connection = null;
            connection = (HttpURLConnection) surl.openConnection();
            connection.setRequestMethod(method);
            connection.setRequestProperty("Content-Type", "application/json");
            if (token){
                connection.setRequestProperty("Authorization", "Bearer "+localStorage.getToken());
            }
            if (! method.equals("GET")){
                connection.setDoOutput(true);
            }
            if(data != null){
                Log.d(TAG, "data");
                OutputStream os = connection.getOutputStream();
                os.write(data.getBytes());
                os.flush();
                os.close();
            }
            statuscode = connection.getResponseCode();
            Log.d(TAG, "conecting : "+ statuscode.toString());


            InputStreamReader isr;
            if (statuscode >= 200 && statuscode <= 299){
                isr = new InputStreamReader(connection.getInputStream());
            }else {
                isr = new InputStreamReader(connection.getErrorStream());
            }

            BufferedReader br = new BufferedReader(isr);
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null){
                sb.append(line);
            }
            Log.d(TAG, "bawah");

            br.close();
            response = sb.toString();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

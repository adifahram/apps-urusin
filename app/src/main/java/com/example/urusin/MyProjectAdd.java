package com.example.urusin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class MyProjectAdd extends AppCompatActivity {

    EditText judul, price_form, price_to, deadline,deskripsi,kota;
    Button btn_save;
    String judultxt, price_formtxt, price_totxt, deadlinetxt, deskripsitxt,kotatxt, kategori;

    String[] items ;
    AutoCompleteTextView autoCompleteTextView;
    ArrayAdapter<String> adapteritems;
    ArrayList<HashMap<String, String>> datas = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_project_add);

        autoCompleteTextView = findViewById(R.id.select_category);

        ListData();
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                kategori = adapterView.getItemAtPosition(i).toString();
                Toast.makeText(getApplicationContext(),"Item: "+kategori, Toast.LENGTH_SHORT).show();
            }
        });


        judul = findViewById(R.id.judul);
        price_form = findViewById(R.id.price_form);
        price_to = findViewById(R.id.price_to);
        deadline = findViewById(R.id.deadline);
        deskripsi = findViewById(R.id.deskripsi);
        kota = findViewById(R.id.kota);
        btn_save = findViewById(R.id.btn_save);


        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInputan();
            }
        });
    }

    private void ListData() {


        String url = getString(R.string.api_server)+"list_category";
        Log.d("logadi", url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("logadi", "cekmasukdialog");
                showProgressDialog();
                Http http = new Http(MyProjectAdd.this, url);
                http.setToken(true);
                http.send();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        Integer code = http.getStatuscode();
                        Log.d("logadi", code.toString());

                        Log.d("logadi", "cek_responses:");
                        Log.d("logadi", http.getResponse().toString());

                        if (code == 200){

                            try {
                                Log.d("logadi","data-data ");

                                JSONObject response = new JSONObject(http.getResponse());
                                JSONArray dataArray = response.getJSONArray("data");

                                String[] names = new String[dataArray.length()];
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject obj = dataArray.getJSONObject(i);
                                    names[i] = obj.getString("name");
                                }


                                adapteritems = new ArrayAdapter<String>(MyProjectAdd.this, R.layout.select_item, names);
                                autoCompleteTextView.setAdapter(adapteritems);

                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }else if (code == 401){
                            Intent intent = new Intent(MyProjectAdd.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }else{
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                String msg = response.getString("message");
                                alertFail(msg);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });


            }
        }).start();


    }




    private void checkInputan() {
        kotatxt = kota.getText().toString();
        judultxt = judul.getText().toString();
        price_formtxt = price_form.getText().toString();
        price_totxt = price_to.getText().toString();
        deadlinetxt = deadline.getText().toString();
        deskripsitxt = deskripsi.getText().toString();
        if (judultxt.isEmpty() || price_formtxt.isEmpty() || kategori.isEmpty() || price_totxt.isEmpty() || deadlinetxt.isEmpty()|| deskripsitxt.isEmpty() || kotatxt.isEmpty()){
            alertFail("All Input is Required");

        }else{
            SimpanData();

        }
    }
    private void SimpanData() {

        JSONObject params = new JSONObject();
        try {
            params.put("kategori", kategori);
            params.put("city", kotatxt);
            params.put("name", judultxt);
            params.put("price_form", price_formtxt);
            params.put("price_to", price_totxt);
            params.put("deadline", deadlinetxt);
            params.put("description", deskripsitxt);

        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        Log.d("logadi", params.toString());

        String data = params.toString();
        String url = getString(R.string.api_server)+"create_project";
        Log.d("logadi", url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("logadi", "cekmasuk");

                showProgressDialog();
                Http http = new Http(MyProjectAdd.this, url);
                http.setMethod("post");
                http.setToken(true);
                http.setData(data);
                http.send();
                Log.d("logadi", http.getResponse());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        Integer code = http.getStatuscode();

                        Log.d("logadi", code.toString());

                        if (code == 200){
                            Intent intent = new Intent(MyProjectAdd.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }else{
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                String msg = response.getString("message");
                                alertFail(msg);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        }).start();

    }


    private void alertFail(String s) {
        new AlertDialog.Builder(this)
                .setTitle("Failed")
                .setIcon(R.drawable.warning)
                .setMessage(s)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }

    private AlertDialog progressDialog;
    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog = new AlertDialog.Builder(MyProjectAdd.this)
                        .setTitle("Loading")
                        .setView(R.layout.loading)
                        .setCancelable(false)
                        .show();
            }
        });
    }

    private void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }
}
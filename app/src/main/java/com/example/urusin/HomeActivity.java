package com.example.urusin;

import static com.google.common.io.Files.getFileExtension;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.constants.ScaleTypes;
import com.denzcoskun.imageslider.interfaces.ItemClickListener;
import com.denzcoskun.imageslider.models.SlideModel;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class HomeActivity extends AppCompatActivity {

    ImageSlider imageSlider;
    Button ppob, project;

    ArrayList<SlideModel> images = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        androidx.appcompat.widget.Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        BottomNavigationView bottomNavigationView = findViewById(R.id.menu_bawah);
        bottomNavigationView.setSelectedItemId(R.id.homes);

        bottomNavigationView.setOnItemSelectedListener(item -> {
            if (item.getItemId() == R.id.profiles){
                Intent intent = new Intent(HomeActivity.this, UserActivity.class);
                startActivity(intent);

            }else if (item.getItemId() == R.id.projects){
                Intent intent = new Intent(HomeActivity.this, MyProjectActivity.class);
                startActivity(intent);
                finish();
            }else if (item.getItemId() == R.id.bids){
                Intent intent = new Intent(HomeActivity.this, MyBidActivity.class);
                startActivity(intent);
                finish();

            }else if (item.getItemId() == R.id.transaksis){
                Intent intent = new Intent(HomeActivity.this, MyTransaksiActivity.class);
                startActivity(intent);
                finish();

            }
            return true;
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, Faq.class));

            }
        });

        project = findViewById(R.id.btnProject);
        project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });


        ppob = findViewById(R.id.bntPpob);
        ppob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(HomeActivity.this, "Coming Soon", Toast.LENGTH_SHORT).show();
            }
        });


        imageSlider = findViewById(R.id.image_slider);



        listData();


        imageSlider.setItemClickListener(new ItemClickListener() {
            @Override
            public void doubleClick(int i) {

            }
            @Override
            public void onItemSelected(int position) {
                SlideModel slide;
                slide = images.get(position);
                String fileExtension = getFileExtension(slide.getImageUrl());
                Log.d("adi", fileExtension);
                if (fileExtension.equals("mp4")) {

                    PlayerView playerView = new PlayerView(HomeActivity.this);
                    SimpleExoPlayer player = new SimpleExoPlayer.Builder(HomeActivity.this).build();
                    playerView.setPlayer(player);

                    // Set video URL
                    MediaItem mediaItem = MediaItem.fromUri(slide.getImageUrl());
                    player.setMediaItem(mediaItem);

                    player.prepare();
                    player.play();

                    AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                    builder.setView(playerView);
                    builder.setPositiveButton("Close", (dialog, which) -> {

                        player.stop();
                        dialog.dismiss();
                    });
                    builder.setCancelable(false);
                    builder.show();
                } else {

                }
            }
        });
    }

    private void listData(){

        String url = getString(R.string.api_server)+"web/sliders";

        Log.d("logadi", url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("logadi", "cekmasukdialog");
                showProgressDialog();
                Http http = new Http(HomeActivity.this, url);
                http.setToken(true);
                http.send();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        Integer code = http.getStatuscode();
                        Log.d("logadi", code.toString());

                        Log.d("logadi", "cek_responses:");
                        Log.d("logadi", http.getResponse().toString());

                        if (code == 200){

                            NumberFormat rupiah = NumberFormat.getInstance(Locale.GERMANY);
                            try {

                                JSONObject response = new JSONObject(http.getResponse());
                                JSONArray dataArray = response.getJSONArray("data");

                                if (dataArray.length() > 0) {
                                    for (int i = 0; i < dataArray.length(); i++) {
                                        // Mendapatkan objek pertama dari array data
                                        JSONObject dataObject = dataArray.getJSONObject(i);
                                        Log.d("logadi",dataObject.toString());

                                        String id = dataObject.getString("id");
                                        String image = dataObject.getString("image");


                                        images.add(new SlideModel(image,null));


                                    }
                                }

                                imageSlider.setImageList(images, ScaleTypes.CENTER_CROP);


                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }else if (code == 401){
                            Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }else{
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                String msg = response.getString("message");
                                alertFail(msg);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });


            }
        }).start();

//
//        images.add(new SlideModel("https://bit.ly/2YoJ77H",null));
//        images.add(new SlideModel("https://bit.ly/2BteuF2", null));
//        images.add(new SlideModel("https://bit.ly/3fLJf72", null));
//        images.add(new SlideModel( "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", null));
//
//
//        imageSlider.setImageList(images, ScaleTypes.CENTER_CROP);
    }


    private android.app.AlertDialog progressDialog;
    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog = new android.app.AlertDialog.Builder(HomeActivity.this)
                        .setTitle("Loading")
                        .setView(R.layout.loading)
                        .setCancelable(false)
                        .show();
            }
        });
    }

    private void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }
    private void alertFail(String s) {
        new android.app.AlertDialog.Builder(HomeActivity.this)
                .setTitle("Failed")
                .setIcon(R.drawable.warning)
                .setMessage(s)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }
}
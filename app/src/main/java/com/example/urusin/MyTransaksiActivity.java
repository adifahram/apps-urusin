package com.example.urusin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;

public class MyTransaksiActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager2 viewPager2;
    ViewPagerAdapterMyTransaksi viewPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_transaksi);


        tabLayout = findViewById(R.id.tablaymytransaksi);
        viewPager2 = findViewById(R.id.view_pager_mytransaksi);

        viewPagerAdapter = new ViewPagerAdapterMyTransaksi(this);
        viewPager2.setAdapter(viewPagerAdapter);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager2.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                tabLayout.getTabAt(position).select();
            }
        });


        androidx.appcompat.widget.Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        BottomNavigationView bottomNavigationView = findViewById(R.id.menu_bawah);
        bottomNavigationView.setSelectedItemId(R.id.transaksis);

        bottomNavigationView.setOnItemSelectedListener(item -> {
            if (item.getItemId() == R.id.homes){
                Intent intent = new Intent(MyTransaksiActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }else if (item.getItemId() == R.id.profiles){
                Intent intent = new Intent(MyTransaksiActivity.this, UserActivity.class);
                startActivity(intent);

            }else if (item.getItemId() == R.id.bids){
                Intent intent = new Intent(MyTransaksiActivity.this, MyBidActivity.class);
                startActivity(intent);
                finish();

            }else if (item.getItemId() == R.id.projects){
                Intent intent = new Intent(MyTransaksiActivity.this, MyProjectActivity.class);
                startActivity(intent);
                finish();

            }
            return true;
        });
    }
}
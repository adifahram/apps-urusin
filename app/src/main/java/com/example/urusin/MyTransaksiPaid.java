package com.example.urusin;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class MyTransaksiPaid extends Fragment {

    public MyTransaksiPaid() {
        // Required empty public constructor
    }
    SwipeRefreshLayout swipeRefreshLayout;

    // variable untuk list data
    ListView list_category;

    public static String url_ = "1";

    ArrayList<HashMap<String, String>> datas = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_my_transaksi_paid, container,false);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ListData();
            }
        });

        list_category = (ListView) view.findViewById(R.id.list_category);


//        list_category.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                url_ = ((TextView) view.findViewById(R.id.link)).getText().toString();
//                Intent intents = new Intent(getActivity(), PaymentGateway.class);
//                startActivity(intents);
//                getActivity().finish();
//            }
//        });

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ListData();
            }
        });



        // Inflate the layout for this fragment
        return view;

    }


    private void ListData() {
        swipeRefreshLayout.setRefreshing(false);
        datas.clear(); list_category.setAdapter(null);
        String url = getString(R.string.api_server)+"my_transaksi_paid";
        Log.d("logadi", url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("logadi", "cekmasukdialog");
                showProgressDialog();
                Http http = new Http(getActivity(), url);
                http.setToken(true);
                http.send();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        Integer code = http.getStatuscode();
                        Log.d("logadi", code.toString());

                        Log.d("logadi", "cek_responses:");
                        Log.d("logadi", http.getResponse().toString());

                        if (code == 200){

                            NumberFormat rupiah = NumberFormat.getInstance(Locale.GERMANY);
                            try {
                                Log.d("logadi","data-data ");

                                JSONObject response = new JSONObject(http.getResponse());
                                JSONArray dataArray = response.getJSONArray("data");

                                if (dataArray.length() > 0) {
                                    for (int i = 0; i < dataArray.length(); i++) {
                                        // Mendapatkan objek pertama dari array data
                                        JSONObject dataObject = dataArray.getJSONObject(i);
                                        JSONObject datatransaksi = dataObject.getJSONObject("transaksi");

                                        // Mengakses nilai-nilai di dalam objek data
                                        String id = dataObject.getString("id");
                                        String link = dataObject.getString("link");
                                        String description = dataObject.getString("description");
                                        String type = datatransaksi.getString("type");
                                        String price_form = rupiah.format(dataObject.getInt("total"));
                                        // ...

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("id", id);
                                        map.put("link", link);
                                        map.put("description",description);
                                        map.put("type",type);
                                        map.put("total",price_form);

                                        datas.add(map);
                                    }
                                }

                                SimpleAdapter simpleAdapter = new SimpleAdapter(getActivity(), datas, R.layout.list_transaksi ,
                                        new String[]{ "id", "link", "description", "type", "total" },
                                        new int[]{R.id.text_transaksi_id, R.id.link, R.id.deskripsi_project, R.id.name_project, R.id.harga_project}) {

                                };

                                list_category.setAdapter(simpleAdapter);

                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }else if (code == 401){
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        }else{
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                String msg = response.getString("message");
                                alertFail(msg);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });


            }
        }).start();


    }

    private AlertDialog progressDialog;
    private void showProgressDialog() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog = new AlertDialog.Builder(getActivity())
                        .setTitle("Loading")
                        .setView(R.layout.loading)
                        .setCancelable(false)
                        .show();
            }
        });
    }

    private void hideProgressDialog() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }
    private void alertFail(String s) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Failed")
                .setIcon(R.drawable.warning)
                .setMessage(s)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }
}
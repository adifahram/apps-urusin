package com.example.urusin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;

public class AcceptProject extends AppCompatActivity {
    Button btn_bid;
    ChatActivity chatActivity = new ChatActivity();
    EditText budget_bid,bider,deadline_bid;
    String budget_bid_str,bider_str,deadline_bid_str;
    public static String urlredirects = "1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_project);

        budget_bid = findViewById(R.id.budget_bid);
        bider = findViewById(R.id.deskripsi_bid);
        deadline_bid = findViewById(R.id.deadline_bid);
        btn_bid = findViewById(R.id.btn_bid);
        ListData();

        btn_bid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInputan();
            }
        });
    }

    private void ListData() {


        String url = getString(R.string.api_server)+"discuss/"+ chatActivity.id_for_bid;
        Log.d("logadi", url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("logadi", "cekmasukdialog");
                showProgressDialog();
                Http http = new Http(AcceptProject.this, url);
                http.setToken(true);
                http.send();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        Integer code = http.getStatuscode();
                        Log.d("logadi", code.toString());

                        Log.d("logadi", "cek_responses:");
                        Log.d("logadi", http.getResponse().toString());

                        if (code == 200){
                            NumberFormat rupiah = NumberFormat.getInstance(Locale.GERMANY);
                            try {
                                Log.d("logadi","data-data ");

                                JSONObject response = new JSONObject(http.getResponse());
                                JSONObject data = response.getJSONObject("data");

                                String descriptions = data.getString("description");
                                String amount_bids = data.getString("amount_bid");
                                String days = data.getString("day");

                                bider.setText(descriptions);
                                budget_bid.setText(amount_bids);
                                deadline_bid.setText(days);



                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }else if (code == 401){
                            Intent intent = new Intent(AcceptProject.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }else{
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                String msg = response.getString("message");
                                alertFail(msg);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });


            }
        }).start();


    }

    private void checkInputan() {
        budget_bid_str = budget_bid.getText().toString();
        bider_str = bider.getText().toString();
        deadline_bid_str = deadline_bid.getText().toString();
        if (budget_bid_str.isEmpty() || bider_str.isEmpty() || deadline_bid_str.isEmpty()){
            alertFail("All Input is Required");

        }else{
            SimpanData();

        }
    }
    private void SimpanData() {

        JSONObject params = new JSONObject();
        try {
            params.put("penawaran", budget_bid_str);
            params.put("deadline", deadline_bid_str);

        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        Log.d("logadi", params.toString());

        String data = params.toString();
        String url = getString(R.string.api_server)+"accept_bid/" + chatActivity.id_for_bid;
        Log.d("logadi", url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("logadi", "cekmasuk");

                showProgressDialog();
                Http http = new Http(AcceptProject.this, url);
                http.setMethod("post");
                http.setToken(true);
                http.setData(data);
                http.send();
                Log.d("logadi", http.getResponse());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        Integer code = http.getStatuscode();

                        Log.d("logadi", code.toString());

                        if (code == 200){
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                JSONObject data = response.getJSONObject("data");

                                String names = data.getString("paymentUrl");
                                urlredirects = names;

                                Intent intents = new Intent(AcceptProject.this, PaymentGateway.class);
                                startActivity(intents);
                                finish();

                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                String msg = response.getString("message");
                                alertFail(msg);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        }).start();

    }


    private void alertFail(String s) {
        new AlertDialog.Builder(this)
                .setTitle("Failed")
                .setIcon(R.drawable.warning)
                .setMessage(s)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }

    private AlertDialog progressDialog;
    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog = new AlertDialog.Builder(AcceptProject.this)
                        .setTitle("Loading")
                        .setView(R.layout.loading)
                        .setCancelable(false)
                        .show();
            }
        });
    }

    private void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }
}
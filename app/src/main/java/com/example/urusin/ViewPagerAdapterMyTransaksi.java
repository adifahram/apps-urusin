package com.example.urusin;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class ViewPagerAdapterMyTransaksi extends FragmentStateAdapter {

    public ViewPagerAdapterMyTransaksi(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0: return new MyTransaksiUnpaid();
            case 1: return new MyTransaksiPaid();
            case 2: return new MyTansaksiRating();
            default:return new MyTransaksiUnpaid();
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}

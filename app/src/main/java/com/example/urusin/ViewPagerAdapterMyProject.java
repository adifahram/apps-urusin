package com.example.urusin;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class ViewPagerAdapterMyProject extends FragmentStateAdapter {

    public ViewPagerAdapterMyProject(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0: return new MyProjectTabProject();
            case 1: return new MyProjectTabProduct();
            case 2: return new MyProjectTabBlog();
            default:return new MyProjectTabProject();
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}

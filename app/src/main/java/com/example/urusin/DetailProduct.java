package com.example.urusin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class DetailProduct extends AppCompatActivity {
    Button btn_bid;
    public static String urlredirectss = "1";

    public static String project_id_for_bid;
    DataProduct dataProject = new DataProduct();
    EditText nama_project,budget_project,deskripsi_project;

    TextView text_transaksi_id;

    GridView list_bider;
    ArrayList<HashMap<String, String>> datas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_product);


        text_transaksi_id = findViewById(R.id.project_id);
        nama_project = findViewById(R.id.nama_project);
        budget_project = findViewById(R.id.budget_project);
        deskripsi_project = findViewById(R.id.deskripsi_project);

        ListData();
        list_bider = (GridView) findViewById(R.id.list_grid_bider);
        project_id_for_bid = "";
        btn_bid = findViewById(R.id.btn_bid);
        btn_bid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SimpanData();
            }
        });

    }


    private void SimpanData() {

        JSONObject params = new JSONObject();

        String data = params.toString();
        String url = getString(R.string.api_server)+"product/beli/" + dataProject.project_id;
        Log.d("logadi", url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("logadi", "cekmasuk");

                showProgressDialog();
                Http http = new Http(DetailProduct.this, url);
                http.setToken(true);
                http.send();
                Log.d("logadi", http.getResponse());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        Integer code = http.getStatuscode();

                        Log.d("logadi", code.toString());

                        if (code == 200){
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                JSONObject data = response.getJSONObject("data");

                                String names = data.getString("paymentUrl");
                                urlredirectss = names;

                                Intent intents = new Intent(DetailProduct.this, PaymentGateway.class);
                                startActivity(intents);
                                finish();

                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                String msg = response.getString("message");
                                alertFail(msg);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        }).start();

    }

    private void ListData() {


        String url = getString(R.string.api_server)+"product/"+ dataProject.project_id;
        Log.d("logadi", url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("logadi", "cekmasukdialog");
                showProgressDialog();
                Http http = new Http(DetailProduct.this, url);
                http.setToken(true);
                http.send();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        Integer code = http.getStatuscode();
                        Log.d("logadi", code.toString());

                        Log.d("logadi", "cek_responses:");
                        Log.d("logadi", http.getResponse().toString());

                        if (code == 200){
                            NumberFormat rupiah = NumberFormat.getInstance(Locale.GERMANY);
                            try {
                                Log.d("logadi","data-data ");

                                JSONObject response = new JSONObject(http.getResponse());
                                JSONObject data = response.getJSONObject("data");

                                String ids = data.getString("id");
                                String names = data.getString("title");
                                String emails = data.getString("description");
                                String price_forms = rupiah.format(data.getInt("price"));
                                text_transaksi_id.setText(ids);
                                nama_project.setText(names);
                                budget_project.setText("Rp. "+price_forms);
                                deskripsi_project.setText(emails);


                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }else if (code == 401){
                            Intent intent = new Intent(DetailProduct.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }else{
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                String msg = response.getString("message");
                                alertFail(msg);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });


            }
        }).start();


    }





    private void alertFail(String s) {
        new AlertDialog.Builder(this)
                .setTitle("Failed")
                .setIcon(R.drawable.warning)
                .setMessage(s)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }

    private AlertDialog progressDialog;
    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog = new AlertDialog.Builder(DetailProduct.this)
                        .setTitle("Loading")
                        .setView(R.layout.loading)
                        .setCancelable(false)
                        .show();
            }
        });
    }

    private void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }
}
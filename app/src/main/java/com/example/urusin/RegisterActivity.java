package com.example.urusin;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity {
    LocalStorage localStorage;

    EditText username, password, email, phone,name,address, city;
    Button btn_login, btn_register;
    String usernametxt, passwordtxt, emailtxt, phonetxt, nametxt,addresstxt, citytxt;
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        localStorage = new LocalStorage(RegisterActivity.this);

        username = findViewById(R.id.usernamer);
        password = findViewById(R.id.passwordr);
        email = findViewById(R.id.emailr);
        phone = findViewById(R.id.phoner);
        name = findViewById(R.id.namer);
        address = findViewById(R.id.address);
        city = findViewById(R.id.city);
        btn_login = findViewById(R.id.btn_loginr);
        btn_register = findViewById(R.id.btn_registerr);

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkLogin();
            }
        });


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void checkLogin() {
        usernametxt = username.getText().toString();
        passwordtxt = password.getText().toString();
        emailtxt = email.getText().toString();
        phonetxt = phone.getText().toString();
        nametxt = name.getText().toString();
        citytxt = city.getText().toString();
        addresstxt = address.getText().toString();
        if (citytxt.isEmpty() ||addresstxt.isEmpty() || usernametxt.isEmpty() || passwordtxt.isEmpty() || emailtxt.isEmpty() || phonetxt.isEmpty()|| nametxt.isEmpty()){
            alertFail("All Input is Required");

        }else{
            String email = emailtxt.trim();

            if (isValidEmail(email)) {
                sendLogin();
            } else {
                alertFail("The email must be a valid email address.");
            }

        }
    }

    private void sendLogin() {

        JSONObject params = new JSONObject();
        try {
            params.put("username", usernametxt);
            params.put("password", passwordtxt);
            params.put("email", emailtxt);
            params.put("phone", phonetxt);
            params.put("name", nametxt);
            params.put("address", addresstxt);
            params.put("city", citytxt);

        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        Log.d("logadi", params.toString());

        String data = params.toString();
        String url = getString(R.string.api_server)+"register";
        Log.d("logadi", url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("logadi", "cekmasuk");

                showProgressDialog();
                Http http = new Http(RegisterActivity.this, url);
                http.setMethod("post");
                http.setData(data);
                http.send();
                Log.d("logadi", http.getResponse());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        Integer code = http.getStatuscode();

                        Log.d("logadi", code.toString());

                        if (code == 200){
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                JSONObject meta = response.getJSONObject("meta");
                                JSONObject data = response.getJSONObject("data");
                                String token = data.getString("token");

                                Log.d("logadi", token);
                                localStorage.setToken(token);
                                Intent intent = new Intent(RegisterActivity.this, HomeActivity.class);
                                startActivity(intent);
                                finish();

                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                String msg = response.getString("message");
                                alertFail(msg);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        }).start();

    }

    // Fungsi untuk memeriksa validitas email menggunakan regular expression
    private boolean isValidEmail(String email) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        return email.matches(emailPattern);
    }
    private void alertFail(String s) {
        new AlertDialog.Builder(this)
                .setTitle("Failed")
                .setIcon(R.drawable.warning)
                .setMessage(s)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }

    private AlertDialog progressDialog;
    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog = new AlertDialog.Builder(RegisterActivity.this)
                        .setTitle("Loading")
                        .setView(R.layout.loading)
                        .setCancelable(false)
                        .show();
            }
        });
    }

    private void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }
}
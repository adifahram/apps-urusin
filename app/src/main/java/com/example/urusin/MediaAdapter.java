package com.example.urusin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.util.List;


public class MediaAdapter extends RecyclerView.Adapter<MediaAdapter.ViewHolder> {

    private Context context;
    private List<MediaFile> mediaFiles;

    public MediaAdapter(Context context, List<MediaFile> mediaFiles) {
        this.context = context;
        this.mediaFiles = mediaFiles;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return new ViewHolder(inflater.inflate(R.layout.view_media, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        File file = new File(mediaFiles.get(position).getUri().getPath());
        holder.tvFilename.setText(file.getName());
        holder.tvPathFile.setText(file.getPath());
    }

    @Override
    public int getItemCount() {
        return mediaFiles.size();
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvFilename,tvPathFile;

        public ViewHolder(View itemView) {
            super(itemView);
            tvFilename = itemView.findViewById(R.id.tvFilename);
            tvPathFile = itemView.findViewById(R.id.tvPathFile);
        }
    }
}

package com.example.urusin;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class ViewPagerAdapterMyBid extends FragmentStateAdapter {

    public ViewPagerAdapterMyBid(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0: return new MyBidTab();
            case 1: return new MyBidOther();
            default:return new MyBidTab();
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}

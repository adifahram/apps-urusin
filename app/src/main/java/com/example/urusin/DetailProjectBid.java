package com.example.urusin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

public class DetailProjectBid extends AppCompatActivity {
    Button btn_bid;
    DataProject dataProject = new DataProject();
    EditText budget_bid,deskripsi_bid,deadline_bid,kota;
    String budget_bid_str,deskripsi_bid_str,deadline_bid_str,kota_str;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_project_bid);

        budget_bid = findViewById(R.id.budget_bid);
        deskripsi_bid = findViewById(R.id.deskripsi_bid);
        deadline_bid = findViewById(R.id.deadline_bid);
        kota = findViewById(R.id.kota);
        btn_bid = findViewById(R.id.btn_bid);

        btn_bid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInputan();
            }
        });
    }

    private void checkInputan() {
        kota_str = kota.getText().toString();
        budget_bid_str = budget_bid.getText().toString();
        deskripsi_bid_str = deskripsi_bid.getText().toString();
        deadline_bid_str = deadline_bid.getText().toString();
        if (budget_bid_str.isEmpty() || deskripsi_bid_str.isEmpty() || kota_str.isEmpty() || deadline_bid_str.isEmpty()){
            alertFail("All Input is Required");

        }else{
            SimpanData();

        }
    }
    private void SimpanData() {

        JSONObject params = new JSONObject();
        try {
            params.put("city", kota_str);
            params.put("amount_bid", budget_bid_str);
            params.put("description", deskripsi_bid_str);
            params.put("day", deadline_bid_str);

        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        Log.d("logadi", params.toString());

        String data = params.toString();
        String url = getString(R.string.api_server)+"project/bid/" + dataProject.project_id;
        Log.d("logadi", url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("logadi", "cekmasuk");

                showProgressDialog();
                Http http = new Http(DetailProjectBid.this, url);
                http.setMethod("post");
                http.setToken(true);
                http.setData(data);
                http.send();
                Log.d("logadi", http.getResponse());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        Integer code = http.getStatuscode();

                        Log.d("logadi", code.toString());

                        if (code == 200){
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                JSONObject data = response.getJSONObject("data");

                                Intent intents = new Intent(DetailProjectBid.this, MainActivity.class);
                                startActivity(intents);
                                finish();

                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                String msg = response.getString("message");
                                alertFail(msg);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        }).start();

    }


    private void alertFail(String s) {
        new AlertDialog.Builder(this)
                .setTitle("Failed")
                .setIcon(R.drawable.warning)
                .setMessage(s)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }

    private AlertDialog progressDialog;
    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog = new AlertDialog.Builder(DetailProjectBid.this)
                        .setTitle("Loading")
                        .setView(R.layout.loading)
                        .setCancelable(false)
                        .show();
            }
        });
    }

    private void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }
}
package com.example.urusin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    // swipe refresh


    TabLayout tabLayout;
    ViewPager2 viewPager2;
    ViewPagerAdapter viewPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabLayout = findViewById(R.id.tablayhome);
        viewPager2 = findViewById(R.id.view_pager_home);
        viewPagerAdapter = new ViewPagerAdapter(this);
        viewPager2.setAdapter(viewPagerAdapter);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager2.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                tabLayout.getTabAt(position).select();
            }
        });


        androidx.appcompat.widget.Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        BottomNavigationView bottomNavigationView = findViewById(R.id.menu_bawah);
        bottomNavigationView.setSelectedItemId(R.id.homes);

        bottomNavigationView.setOnItemSelectedListener(item -> {
            if (item.getItemId() == R.id.profiles){
                Intent intent = new Intent(MainActivity.this, UserActivity.class);
                startActivity(intent);

            }else if (item.getItemId() == R.id.projects){
                Intent intent = new Intent(MainActivity.this, MyProjectActivity.class);
                startActivity(intent);
                finish();
            }else if (item.getItemId() == R.id.bids){
                Intent intent = new Intent(MainActivity.this, MyBidActivity.class);
                startActivity(intent);
                finish();

            }else if (item.getItemId() == R.id.transaksis){
                Intent intent = new Intent(MainActivity.this, MyTransaksiActivity.class);
                startActivity(intent);
                finish();

            }else if (item.getItemId() == R.id.transaksis){
                Intent intent = new Intent(MainActivity.this, MyTransaksiActivity.class);
                startActivity(intent);
                finish();

            }
            return true;
        });








    }



    @Override
    public void onResume() {
        super.onResume();

        BottomNavigationView bottomNavigationView = findViewById(R.id.menu_bawah);
        bottomNavigationView.setSelectedItemId(R.id.homes);


    }


}
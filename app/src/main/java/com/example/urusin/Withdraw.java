package com.example.urusin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

public class Withdraw extends AppCompatActivity {
    EditText nominal;
    UserActivity datasaldo = new UserActivity();

    Button btn_save;
    String nominaltxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw);

        nominal = findViewById(R.id.nominal);
        btn_save = findViewById(R.id.btn_update);


        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInputan();
            }
        });


    }
    @Override
    public void onResume() {
        super.onResume();
        Log.d("logadi", datasaldo.mysaldo);


        nominal.setText(datasaldo.mysaldo);

    }

    private void checkInputan() {
        nominaltxt = nominal.getText().toString();
        if (nominaltxt.isEmpty()) {
            alertFail("All Input is Required");
        } else {
            if (nominaltxt.compareTo(datasaldo.mysaldo) > 0) {
                alertFail("Harus Kurang Dari saldo yang anda miliki");
            } else {
                SimpanData();
            }
        }
    }
    private void SimpanData() {

        JSONObject params = new JSONObject();
        try {
            params.put("request_nominal", nominaltxt);
            params.put("withdraw", "yes");

        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        Log.d("logadi", params.toString());

        String data = params.toString();
        String url = getString(R.string.api_server)+"profil";
        Log.d("logadi", url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("logadi", "cekmasuk");

                showProgressDialog();
                Http http = new Http(Withdraw.this, url);
                http.setMethod("put");
                http.setToken(true);
                http.setData(data);
                http.send();
                Log.d("logadi", http.getResponse());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();

                        Integer code = http.getStatuscode();

                        Log.d("logadi", code.toString());

                        if (code == 200){

                            Intent intents = new Intent(Withdraw.this, HomeActivity.class);
                            startActivity(intents);
                            finish();
                        }else{
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                String msg = response.getString("message");
                                alertFail(msg);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        }).start();

    }


    private void alertFail(String s) {
        new AlertDialog.Builder(this)
                .setTitle("Failed")
                .setIcon(R.drawable.warning)
                .setMessage(s)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }

    private AlertDialog progressDialog;
    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog = new AlertDialog.Builder(Withdraw.this)
                        .setTitle("Loading")
                        .setView(R.layout.loading)
                        .setCancelable(false)
                        .show();
            }
        });
    }

    private void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }
}
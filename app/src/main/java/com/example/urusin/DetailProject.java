package com.example.urusin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class DetailProject extends AppCompatActivity {

    Button btn_bid;

    public static String project_id_for_bid;
    DataProject dataProject = new DataProject();
    EditText nama_client,nama_project,budget_project,deskripsi_project;

    TextView text_transaksi_id;

    GridView list_bider;
    ArrayList<HashMap<String, String>> datas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_project);

        text_transaksi_id = findViewById(R.id.project_id);
        nama_project = findViewById(R.id.nama_project);
        budget_project = findViewById(R.id.budget_project);
        deskripsi_project = findViewById(R.id.deskripsi_project);
        nama_client = findViewById(R.id.nama_client);

        ListData();
        list_bider = (GridView) findViewById(R.id.list_grid_bider);
        project_id_for_bid = "";
        btn_bid = findViewById(R.id.btn_bid);
        btn_bid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                project_id_for_bid = ((TextView) view.findViewById(R.id.project_id)).getText().toString();
                Intent intent = new Intent(DetailProject.this, DetailProjectBid.class);
                startActivity(intent);
            }
        });
    }

    private void ListData() {


        String url = getString(R.string.api_server)+"project/"+ dataProject.project_id;
        Log.d("logadi", url);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("logadi", "cekmasukdialog");
                showProgressDialog();
                Http http = new Http(DetailProject.this, url);
                http.setToken(true);
                http.send();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        Integer code = http.getStatuscode();
                        Log.d("logadi", code.toString());

                        Log.d("logadi", "cek_responses:");
                        Log.d("logadi", http.getResponse().toString());

                        if (code == 200){
                            NumberFormat rupiah = NumberFormat.getInstance(Locale.GERMANY);
                            try {
                                Log.d("logadi","data-data ");

                                JSONObject response = new JSONObject(http.getResponse());
                                JSONObject data = response.getJSONObject("data");
                                JSONArray dataArray = data.getJSONArray("bids");

                                String ids = data.getString("id");
                                String names = data.getString("name");
                                String emails = data.getString("description");
                                String price_forms = rupiah.format(data.getInt("price_form"));
                                String price_tos = rupiah.format(data.getInt("price_to"));
                                JSONObject clientobj = data.getJSONObject("client");
                                String clients = clientobj.getString("name");

                                text_transaksi_id.setText(ids);
                                nama_project.setText(names);
                                budget_project.setText("Rp. "+price_forms+" - Rp. "+price_tos);
                                deskripsi_project.setText(emails);
                                nama_client.setText(clients);

                                if (dataArray.length() > 0) {
                                    for (int i = 0; i < dataArray.length(); i++) {
                                        // Mendapatkan objek pertama dari array data
                                        JSONObject dataObject = dataArray.getJSONObject(i);
                                        JSONObject userObject = dataObject.getJSONObject("user");

                                        String name = userObject.getString("name");
                                        String email = userObject.getString("email");
                                        // ...

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("name", name);
                                        map.put("email", email);
                                        datas.add(map);
                                    }
                                }

                                SimpleAdapter simpleAdapter = new SimpleAdapter(DetailProject.this, datas, R.layout.list_bider ,
                                        new String[]{  "name" },
                                        new int[]{R.id.text_status}) {

                                };

                                list_bider.setAdapter(simpleAdapter);

                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }else if (code == 401){
                            Intent intent = new Intent(DetailProject.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }else{
                            try {
                                JSONObject response = new JSONObject(http.getResponse());
                                String msg = response.getString("message");
                                alertFail(msg);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });


            }
        }).start();


    }





    private void alertFail(String s) {
        new AlertDialog.Builder(this)
                .setTitle("Failed")
                .setIcon(R.drawable.warning)
                .setMessage(s)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }

    private AlertDialog progressDialog;
    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog = new AlertDialog.Builder(DetailProject.this)
                        .setTitle("Loading")
                        .setView(R.layout.loading)
                        .setCancelable(false)
                        .show();
            }
        });
    }

    private void hideProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }
}